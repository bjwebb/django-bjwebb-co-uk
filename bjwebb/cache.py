from django.core.cache.backends.memcached import MemcachedCache

class VersionedCache(MemcachedCache):
    def __getattr__(self, name): 
        if name == 'version':
            return self.get('version', version=1) or 1 

    def __init__(self, *args, **kwargs):
        super(VersionedCache, self).__init__(*args, **kwargs)
        del self.version

