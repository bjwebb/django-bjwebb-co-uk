from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^openid/', include('openid_provider.urls')),
)

urlpatterns += patterns('bjwebb.views',
    url(r'^p/$', 'projects'), 
    url(r'^p/([-\w\d]+)/$', 'project'), 
    url(r'^posts/$', 'posts'), 
    url(r'^p/([-\w\d]+)/post/([-\w\d]+)/$', 'post'), 
    url(r'^events/$', 'events'), 
    url(r'^p/([-\w\d]+)/event/([-\w\d]+)/$', 'event'), 
)

urlpatterns += patterns('django.contrib.flatpages.views',
    url('^()$', 'flatpage', name='home'),
    url('^(.*/)$', 'flatpage', name='flatpage'),
)

