from django.db import models
from taggit.managers import TaggableManager

class TimeSpan(models.Model):
    class Meta:
        abstract = True
        ordering = ('-ended', '-started', )
    started = models.DateField(null=True, blank=True)
    ended = models.DateField(null=True, blank=True)
    ignore_days = models.BooleanField(default=False)
    
class Project(TimeSpan):
    name = models.CharField(max_length=255, default='')
    slug = models.SlugField()
    text = models.TextField(default='')
    tags = TaggableManager(blank=True)

    external_url = models.CharField(max_length=255, default='', blank=True)
    external_url_name = models.CharField(max_length=255, default='', blank=True)
    source_url = models.TextField(default='', blank=True)
    paid = models.BooleanField(default=False)

    parent = models.ForeignKey('Project', null=True, blank=True)

    def __unicode__(self):
        return self.name

    def source_url_list(self):
        urls = filter(lambda x: x!='', self.source_url.split('\n'))
        return [ (x,x.split('/')[-1])  for x in urls  ]

class Post(models.Model):
    title = models.CharField(max_length=255, default='')
    slug = models.SlugField()
    text = models.TextField(default='')
    tags = TaggableManager(blank=True)
    
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    project = models.ForeignKey(Project, null=True)

    def __unicode__(self):
        return self.title

class Event(TimeSpan):
    name = models.CharField(max_length=255, default='')
    slug = models.SlugField()
    text = models.TextField(default='')
    tags = TaggableManager(blank=True)

    project = models.ForeignKey(Project, null=True)
    external_url = models.CharField(max_length=255, default='', blank=True)
    external_url_name = models.CharField(max_length=255, default='', blank=True)

    def __unicode__(self):
        return self.name


from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.cache import cache

@receiver(post_save)
def bump_version(sender, **kwargs):
    cache.set('version', cache.version+1, 0, version=1)
