from django.contrib import admin
import bjwebb.models as m

class ProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}

class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(m.Project, ProjectAdmin)
admin.site.register(m.Post, PostAdmin)
admin.site.register(m.Event, EventAdmin)
